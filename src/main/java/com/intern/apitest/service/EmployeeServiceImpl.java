package com.intern.apitest.service;

import com.intern.apitest.entity.Employee;
import com.intern.apitest.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee insertEmployee(Employee employee){
        return (Employee) employeeRepository.save(employee);
    }

    public List<Employee> getAllEmployees(){
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id){
        return (Employee) employeeRepository.findById(id).get();
    }

    public Employee updateEmployee(Long id,Employee employee){
        if(employeeRepository.findById(id).isPresent()) {
            return (Employee) employeeRepository.save(employee);
        }
        return null;
    }

    public void deleteEmployee(Long id){
        if(employeeRepository.findById(id).isPresent()){
            employeeRepository.deleteById(id);
        }
    }
}
