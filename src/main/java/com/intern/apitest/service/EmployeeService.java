package com.intern.apitest.service;

import com.intern.apitest.entity.Employee;

import java.util.List;

public interface EmployeeService {
    Employee insertEmployee(Employee employee);
    List<Employee> getAllEmployees();
    Employee getEmployeeById(Long id);
    Employee updateEmployee(Long id,Employee employee);
    void deleteEmployee(Long id);
}
